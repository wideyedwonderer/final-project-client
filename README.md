![img](./src/assets/mainBG.jpg 'Chronos')

<center><h1>Chronos</h1></center>
Angular based UI for traffic engineers, which provide visualization of ETA between pre selected intersections.

#Technologies
Angular, Angular Material,TypeScript, NestJs, Node.js, Jest, MariaDB, TypeORM, Passport, JWT, Moment.js, AmCharts, Toastr, TSLInt, Digital Ocean, Netlify
