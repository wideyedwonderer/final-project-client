import { AdminLogsResolver } from './resolvers/admin-logs-resolver';
import { AdminUsersResolver } from './resolvers/admin-users.resolver';
import { DevicesResolver } from './resolvers/devices.resolver';
import { AdminComponent } from './admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', component: AdminComponent, resolve: {devices: DevicesResolver, users: AdminUsersResolver, logs: AdminLogsResolver} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
