import { EditDeviceComponent } from './edit-device/edit-device.component';

import { AreYouSureComponent } from './../common/are-you-sure/are-you-sure.component';
import { FormsModule } from '@angular/forms';
import {
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatSortModule,
  MatPaginatorModule,
} from '@angular/material';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddDeviceComponent } from './add-device/add-device.component';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTableModule } from '@angular/material/table';
import { AddUserComponent } from './add-user/add-user.component';
import { SharedModule } from '../common/shared.module';

@NgModule({
  declarations: [AdminComponent, AddDeviceComponent, AddUserComponent, EditDeviceComponent],
  imports: [
    FormsModule,
    CommonModule,
    AgmCoreModule,
    AgmDirectionModule,
    MatDialogModule,
    MatFormFieldModule,
    AdminRoutingModule,
    MatExpansionModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    SharedModule,
    MatSortModule,
    MatPaginatorModule
    // MaterialRootModule
  ],
  entryComponents: [AddDeviceComponent, AddUserComponent, AreYouSureComponent, EditDeviceComponent],
})
export class AdminModule {}
