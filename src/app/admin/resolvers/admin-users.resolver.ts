
import { Injectable } from '@angular/core';
import { DbService } from '../../core/db.service';
import { Resolve } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AdminUsersResolver  implements Resolve<any> {


constructor(private readonly db: DbService) {}
  resolve(): Promise<any> {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      return  this.db.getAllUsers().toPromise().then((data: any) => data);
    } else {
      return null;
    }
  }
}
