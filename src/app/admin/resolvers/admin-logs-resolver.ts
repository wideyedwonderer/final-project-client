import { LogService } from './../../core/log.service';

import { Injectable } from '@angular/core';
import { DbService } from '../../core/db.service';
import { Resolve } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AdminLogsResolver  implements Resolve<any> {


constructor(private readonly logger: LogService) {}
  resolve(): Promise<any> {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      return  this.logger.getAllLogs().toPromise().then(logs => logs);
    } else {
      return null;
    }
  }
}
