import { Injectable } from '@angular/core';
import { DbService } from '../../core/db.service';
import { Resolve } from '@angular/router';
import { UserService } from '../../core/user.service';
import { Device } from '../device.interface';

@Injectable({
  providedIn: 'root',
})
export class DevicesResolver implements Resolve<any> {
  constructor(
    private readonly db: DbService,
    private readonly userService: UserService
  ) {}
  resolve(): Promise<Device[]> {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      if (user.role === 'user') {
        return this.db
          .getAllDevices(user.adminId.userId)
          .toPromise()
          .then((data: Device[]) => data);
      } else {
        return this.db
          .getAllDevices(user.userId)
          .toPromise()
          .then((data: Device[]) => data);
      }
    } else {
      return null;
    }
  }
}
