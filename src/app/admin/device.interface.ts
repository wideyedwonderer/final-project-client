export interface Device {
  deviceId?: string;
  lat?: number;
  lng?: number;
  name?: string;
}
