import { IDevice } from './../../core/device.interface';
import { DbService } from 'src/app/core/db.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { mapStyles } from 'src/app/main/map/map-styles';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.scss']
})
export class AddDeviceComponent implements OnInit {
  styles: any;
  mapLat = 42.664879;
  mapLng = 23.317603;
  lat;
  lng;
  devices;
  deviceName;
  currentDeviceCoordinates = {lat: null, lng: null};
   constructor(public dialogRef: MatDialogRef<AddDeviceComponent>,
     @Inject(MAT_DIALOG_DATA) devices: IDevice[],
     private readonly dbService: DbService,
     ) {
       this.devices = devices || [];
       this.styles = mapStyles;
     }

   ngOnInit() {
   }
   onNoClick(): void {
     this.dialogRef.close();
   }
   onSubmit(name) {
     const user = JSON.parse(localStorage.getItem('user'));
     this.dbService.addNewDevice(
       {name: name.value,
        lat: this.currentDeviceCoordinates.lat,
        lng: this.currentDeviceCoordinates.lng,
        userId: user.userId
      }).subscribe();
     this.dialogRef.close();
   }


   click(event) {
     this.currentDeviceCoordinates = {lat: event.coords.lat, lng: event.coords.lng};
   }

}

