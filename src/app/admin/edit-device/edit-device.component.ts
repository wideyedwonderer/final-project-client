import { IDevice } from '../../core/device.interface';
import { DbService } from 'src/app/core/db.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { mapStyles } from 'src/app/main/map/map-styles';
import { Device } from '../device.interface';


@Component({
  selector: 'app-edit-device',
  templateUrl: './edit-device.component.html',
  styleUrls: ['./edit-device.component.scss']
})
export class EditDeviceComponent implements OnInit {
  styles: any;
  mapLat = 42.664879;
  mapLng = 23.317603;
  deviceName;
  device: Device;
  currentDeviceCoordinates = {lat: null, lng: null};
   constructor(public dialogRef: MatDialogRef<EditDeviceComponent>,
     @Inject(MAT_DIALOG_DATA) device: any,
     private readonly dbService: DbService,
     ) {

       this.device = JSON.parse(JSON.stringify(device.element));
       this.styles = mapStyles;
     }

   ngOnInit() {
   }
   onNoClick(): void {
     this.dialogRef.close();
   }
   onSubmit() {
     const user = JSON.parse(localStorage.getItem('user'));
     this.dbService.updateDevice(this.device.deviceId, {name: this.device.name,
      lat: this.device.lat,
      lng: this.device.lng,
      deviceId: this.device.deviceId,
    }).subscribe();
     this.dialogRef.close(this.device);
   }


   click(event) {
     this.device.lat = event.coords.lat;
     this.device.lng = event.coords.lng;
   }

}

