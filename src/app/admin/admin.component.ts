import { EditDeviceComponent } from './edit-device/edit-device.component';

import { AreYouSureComponent } from './../common/are-you-sure/are-you-sure.component';
import { DbService } from '../../app/core/db.service';
import { AddUserComponent } from './add-user/add-user.component';
import { AddDeviceComponent } from './add-device/add-device.component';
import { MatDialog, MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatTable } from '@angular/material';
import { UpdateDevice } from '../models/update-device.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  devices: MatTableDataSource<any>;
  users: MatTableDataSource<any>;
  logs: MatTableDataSource<any>;
  displayedColumnsDevices: string[] = ['name', 'lat', 'lng', 'edit', 'remove'];
  displayedColumnsUsers: string[] = [
    'firstName',
    'lastName',
    'email',
    'status',
  ];
  displayedColumnsLogs: string[] = ['type', 'description', 'date'];
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSort) sortLogs: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private readonly route: ActivatedRoute,
    public dialog: MatDialog,
    private readonly db: DbService
  ) {
    this.devices = new MatTableDataSource(this.route.snapshot.data['devices']);
    this.users = new MatTableDataSource(this.route.snapshot.data['users']);
    this.logs = new MatTableDataSource(this.route.snapshot.data['logs']);
  }

  ngOnInit() {
    setTimeout(() => {
      this.table.renderRows();
    }, 100);
    this.devices.sort = this.sort;
    this.logs.sort = this.sortLogs;
    this.logs.paginator = this.paginator;
  }
  openAddDeviceDialog(): void {
    const dialogRef = this.dialog.open(AddDeviceComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.table.renderRows();
    });
  }

  openAddUserDialog() {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '350px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.table.renderRows();
    });
  }
  switchUserBan(user) {
    const dialogRef = this.dialog.open(AreYouSureComponent, {
      width: '250px',
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.db.switchUserBan(user.email, user.banned).subscribe();
        this.users.data = this.users.data.map((oldUser) => {
          if (oldUser.email === user.email) {
            oldUser.banned = !oldUser.banned;
          }
          return oldUser;
        });
        this.table.renderRows();
      }
    });
  }

  removeDevice(element) {

    const dialogRef = this.dialog.open(AreYouSureComponent, {
      width: '250px',

    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.db.updateDevice(element, {}).subscribe();
      }
    });


  }

  editDevice(element) {
    const dialogRef = this.dialog.open(EditDeviceComponent, {
      width: '400px',
      data: {element}
    });

    dialogRef.afterClosed().subscribe((result) => {
      element.name = result.name;
      element.lat = result.lat;
      element.lng = result.lng;
      this.table.renderRows();
    });


  }
}
