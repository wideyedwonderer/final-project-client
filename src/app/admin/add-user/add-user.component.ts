import { UserService } from './../../core/user.service';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AddUserComponent>,
    private readonly userService: UserService,
    ) {}

  ngOnInit() {
  }

}
