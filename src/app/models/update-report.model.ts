export class UpdateReport {
  public endDate?: string;
  public startDate?: string;
  public name?: string;
}
