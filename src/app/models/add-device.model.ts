export class AddDevice {
  public lat: string;
  public lng: string;
  public name: string;
  public userId: string;
}
