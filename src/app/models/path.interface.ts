export interface IPath {
  id: string;
  min: number;
  max: number;
  current: number;
  active: boolean;
  color?: string;
  name?: string;
}
