export class UpdateDevice {
  public lat?: string;
  public lng?: string;
  public name?: string;
}
