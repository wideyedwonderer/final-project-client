export class User {
  public userId: string;
  public firstName: string;
  public lastName: string;
  public email: string;
  public role: string;
  public adminId: string;
}
