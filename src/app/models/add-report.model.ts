export class AddReport {
  public endDate: string;
  public startDate: string;
  public name: string;
  public userId: string;
}
