import { FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { UserService } from '../../core/user.service';


@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['user-login.component.css']
})

export class UserLoginComponent {

  userLoginForm = this.fb.group({

            email: [null, [Validators.required, Validators.email]],

            password: [null, [Validators.required,
                              Validators.minLength(7),
                              Validators.pattern(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]/)]],
  });
  constructor(private readonly userService: UserService, private readonly fb: FormBuilder) {}

  onSubmit() {
    this.userService.login(this.userLoginForm.value);
  }

  parseError(fieldName): string {
    const field = this.userLoginForm.controls[fieldName];
    switch (fieldName) {
      case 'email': {
        if (field.hasError('required')) {
          return `Email is required`;
        }
        if (field.hasError('email')) {
          return `Email is not valid`;
        }
        break;
      }

      case 'password': {
        if (field.hasError('minlength')) {
          return `The password must be at least 8 characters long`;
        }
        if (field.hasError('pattern')) {
          return `The password must have at least one number, one uppercase letter and a special character`;
        }
        break;
      }
  }
}
}
