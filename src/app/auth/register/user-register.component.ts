import {
  FormBuilder,
  Validators,
  AbstractControl,
  FormGroup,
} from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

import { debounceTime } from 'rxjs/operators';
import { UserService } from '../../../app/core/user.service';
import { User } from '../../../app/models/user.model';
import { Router } from '@angular/router';

function isMatching(controlName1: string, controlName2: string) {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    const control1 = c.get(controlName1);
    const control2 = c.get(controlName2);
    if (control1.value === control2.value) {
      return null;
    }
    return { match: true };
  };
}

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css'],
})
export class UserRegisterComponent implements OnInit {
  currentUser: User;
  @Input()
  isAdminRegisterForm = true;

  userRegisterForm = this.fb.group({
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    secret: [null, Validators.pattern(/^Praskovka$/)],
    emailGroup: this.fb.group(
      {
        email: [null, [Validators.required, Validators.email]],
        confirmEmail: [null, [Validators.required]],
      },
      {
        validator: isMatching('email', 'confirmEmail'),
      }
    ),

    passwordGroup: this.fb.group(
      {
        password: [
          null,
          [
            Validators.required,
            Validators.minLength(7),
            Validators.pattern(
              /(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]/
            ),
          ],
        ],
        confirmPassword: [null, [Validators.required]],
      },
      {
        validator: isMatching('password', 'confirmPassword'),
      }
    ),
  });

  errors = {
    firstName: null,
    lastName: null,
    emailGroup: null,
    email: null,
    confirmEmail: null,
    passwordGroup: null,
    password: null,
    confirmPassword: null,
    secret: null,
  };

  constructor(
    private readonly fb: FormBuilder,
    private readonly userService: UserService,
    private readonly router: Router
  ) {}

  onSubmit() {
    const userToRegister = {
      firstName: this.userRegisterForm.value['firstName'],
      lastName: this.userRegisterForm.value['lastName'],
      email: this.userRegisterForm.value['emailGroup']['email'],
      password: this.userRegisterForm.value['passwordGroup']['password'],
    };

    if (this.userRegisterForm.value['secret']) {
      userToRegister['secret'] = this.userRegisterForm.value['secret'];
    } else {
      userToRegister['adminId'] = this.currentUser.userId;
    }

    this.userService.register(userToRegister).subscribe({
      next: (value) => {
        if (this.userRegisterForm) {
          this.router.navigate(['home']);
        } else {
          this.router.navigate(['admin']);
        }
      },
    });
  }
  public redirectHome() {
    this.router.navigate(['/home']);
  }
  ngOnInit(): void {
    this.userService.currentUser$.subscribe(
      (user) => (this.currentUser = user)
    );
    this.userRegisterForm.controls['firstName'].valueChanges
      .pipe(debounceTime(1500))
      .subscribe(() => {
        this.errors['firstName'] = null;
        if (this.userRegisterForm.controls['firstName'].invalid) {
          this.errors.firstName = this.parseError('firstName');
        }
      });

    this.userRegisterForm.controls['lastName'].valueChanges
      .pipe(debounceTime(1500))
      .subscribe(() => {
        this.errors['lastName'] = null;
        if (this.userRegisterForm.controls['lastName'].invalid) {
          this.errors.lastName = this.parseError('lastName');
        }
      });
    this.userRegisterForm.controls['secret'].valueChanges
      .pipe(debounceTime(3000))
      .subscribe(() => {
        this.errors['secret'] = null;
        if (this.userRegisterForm.controls['secret'].invalid) {
          this.errors.secret = this.parseError('secret');
        }
      });

    (<FormGroup>this.userRegisterForm.controls['emailGroup']).controls[
      'email'
    ].valueChanges
      .pipe(debounceTime(1500))
      .subscribe((value) => {
        this.errors['email'] = null;
        if (this.userRegisterForm.controls['emailGroup'].invalid) {
          this.errors['email'] = this.parseError('email', 'emailGroup');
        }
      });

    (<FormGroup>this.userRegisterForm.controls['emailGroup']).controls[
      'confirmEmail'
    ].valueChanges
      .pipe(debounceTime(1500))
      .subscribe((value) => {
        this.errors['emailGroup'] = null;
        if (this.userRegisterForm.controls['emailGroup'].invalid) {
          this.errors['emailGroup'] = this.parseError('emailGroup');
        }
      });

    (<FormGroup>this.userRegisterForm.controls['passwordGroup']).controls[
      'password'
    ].valueChanges
      .pipe(debounceTime(1500))
      .subscribe((value) => {
        this.errors.password = null;
        if (
          (<FormGroup>this.userRegisterForm.controls['passwordGroup']).controls[
            'password'
          ].invalid
        ) {
          this.errors['password'] = this.parseError(
            'password',
            'passwordGroup'
          );
        }
      });

    (<FormGroup>this.userRegisterForm.controls['passwordGroup']).controls[
      'confirmPassword'
    ].valueChanges
      .pipe(debounceTime(1500))
      .subscribe((value) => {
        this.errors.passwordGroup = null;
        if (this.userRegisterForm.controls['passwordGroup'].invalid) {
          this.errors['passwordGroup'] = this.parseError('passwordGroup');
        }
      });
  }

  parseError(fieldName, subGroupName?): string {
    const field =
      this.userRegisterForm.controls[fieldName] ||
      (<FormGroup>this.userRegisterForm.controls[subGroupName]).controls[
        fieldName
      ];
    switch (fieldName) {
      case 'firstName': {
        if (field.hasError('required')) {
          return `First Name is required`;
        }
        break;
      }
      case 'lastName': {
        if (field.hasError('required')) {
          return `Last Name is required`;
        }
        break;
      }
      case 'secret': {
        if (field.hasError('pattern')) {
          return `Incorrect secret`;
        }
        break;
      }
      case 'email': {
        if (field.hasError('required')) {
          return `Email is required`;
        }
        if (field.hasError('email')) {
          return `Email is not valid`;
        }
        break;
      }
      case 'emailGroup': {
        if (field.hasError('match')) {
          return `Emails do not match`;
        }
        break;
      }
      case 'password': {
        if (field.hasError('minlength')) {
          return `The password must be at least 8 characters long`;
        }
        if (field.hasError('pattern')) {
          return `The password must have at least one number, one uppercase letter and a special character`;
        }
        break;
      }
      case 'passwordGroup': {
        if (field.hasError('match')) {
          return `Passwords do not match`;
        }
        break;
      }
    }
  }
}
