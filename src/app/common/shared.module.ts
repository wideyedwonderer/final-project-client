import { MaterialRootModule } from './material-root.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UserRegisterComponent } from './../auth/register/user-register.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreYouSureComponent } from './are-you-sure/are-you-sure.component';

@NgModule({
  declarations: [UserRegisterComponent, AreYouSureComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialRootModule,
    FormsModule,
  ],
  exports: [UserRegisterComponent, FormsModule]
})
export class SharedModule { }
