import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificatorService } from '../core/notificator.service';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {

  constructor(private readonly router: Router, private readonly notificator: NotificatorService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authedReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    });
    return next.handle(authedReq)
     .pipe(
       catchError((error: HttpErrorResponse) => {
         let errorMessage = '';
         if (error.error instanceof ErrorEvent) {
           // client-side error
           errorMessage = `Error: ${error.error.message}`;
         } else {
           // server-side error
           if (error.status !== 200 && error.status !== 201) {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
           } else {
             errorMessage = `1`;
           }
           if (error.status === 401) {
             this.router.navigate(['home']);
           }
         }
         if (errorMessage === `1`) {
           this.notificator.success('Success');
         } else {
           this.notificator.error(errorMessage);
         }
         return throwError(errorMessage);
       })
     );
  }

}
