import { DataPasserService } from './../../core/data-passer';
import { ActivatedRoute } from '@angular/router';
import { ISlot } from './arrows-tile/slot.interface';
import { IPath } from '../../models/path.interface';
import { PathHelperService } from './../../core/path-helper.service';
import { ApiService } from './../../core/api.service';
import {
  Component,
  OnInit,
  EventEmitter,
  OnDestroy,
  ɵConsole,
} from '@angular/core';
import * as moment from 'moment';
import { DbService } from 'src/app/core/db.service';
import { MatDialog } from '@angular/material';
import { Subscription, interval } from 'rxjs';
import { flatMap } from 'rxjs/operators';

@Component({
  selector: 'app-middle',
  templateUrl: './middle.component.html',
  styleUrls: ['./middle.component.scss'],
})
export class MiddleComponent implements OnInit, OnDestroy {
  devices = [];
  downPathsNames;
  upPathsNames;
  downSlots: Array<ISlot> = [];
  upSlots: Array<ISlot> = [];
  render = false;
  travelTimeSub: Subscription;
  slotsChanged: EventEmitter<boolean> = new EventEmitter();

  paths: { down: IPath[]; up: IPath[] };

  switchesDownOn: boolean[] = [];
  switchesUpOn: boolean[] = [];

  timeSub: Subscription;
  constructor(
    public dialog: MatDialog,
    private readonly apiService: ApiService,
    private readonly pathHelper: PathHelperService,
    private readonly db: DbService,
    private readonly route: ActivatedRoute,
    private readonly dataPasser: DataPasserService
  ) {
    this.paths = this.route.snapshot.data['paths'];
    this.dataPasser.currentRoute$.subscribe((path: any) => {
      this.devices = JSON.parse(path.devices).map((device) => device.name);
    });
  }

  ngOnInit() {
    this.dataPasser.mainInitialized$.next(true);

    this.timeSub = this.dataPasser.timePicked$.subscribe((value: any) => {
      if (value) {
        const [quantity, type] = value.split('-');
        this.setNewSlotsTime(quantity, type);
      }
    });
    this.setSlots();
  }

  setNewSlotsTime(timeQuantity, timeType) {
    if (this.travelTimeSub) {
      this.travelTimeSub.unsubscribe();
    }
    // this.travelTimeSub = interval(1000 * 3).pipe(
    //   flatMap(() =>
    this.apiService
      .getTravelTimeData(
        [...this.devices],
        moment().subtract(timeQuantity, timeType)
      )
      // ) )
      .subscribe((paths) => {
        this.paths = paths;
        this.render = false;
        this.setSlots();
      });
  }

  setSlots() {
    this.downPathsNames = this.paths.down.reduce((acc, path) => {
      acc.push(path.id);
      return acc;
    }, []);
    this.upPathsNames = this.paths.up.reduce((acc, path) => {
      acc.push(path.id);
      return acc;
    }, []);

    this.db.getPathsSettings(this.downPathsNames).subscribe((settings) => {
      this.paths.down = this.paths.down.map((path, index) => {
        path = { ...path, ...settings[index] };
        return path;
      });
      this.paths.down = this.pathHelper.calculatePathsColor([
        ...this.paths.down,
      ]);
      this.downSlots = this.pathHelper.calculateWhereToPutNumbers(
        [...this.devices],
        [...this.paths.down]
      );
      this.switchesDownOn = Array.from(
        { length: this.devices.length },
        (x) => true
      );
      this.downSwitch({ value: true, switchIndex: 1 });
    });
    this.db.getPathsSettings(this.upPathsNames).subscribe((settings) => {
      this.paths.up = this.paths.up.map((path, index) => {
        path = { ...path, ...settings[index] };
        return path;
      });
      this.paths.up = this.pathHelper.calculatePathsColor([...this.paths.up]);
      this.upSlots = this.pathHelper
        .calculateWhereToPutNumbers([...this.devices].reverse(), [
          ...this.paths.up,
        ])
        .reverse();
      this.switchesUpOn = Array.from(
        { length: this.devices.length },
        (x) => true
      );
      this.upSwitch({ value: true, switchIndex: 1 });
    });
  }

  arrowHeight() {
    return (
      (window.innerHeight - this.devices.length * 37 - 62) /
        (this.devices.length - 1) +
      'px'
    );
  }

  upSwitch(emitted?: { value: boolean; switchIndex: number }) {
    if (emitted) {
      this.switchesUpOn[emitted.switchIndex] = emitted.value;
    }
    const pathsToActivate = this.pathHelper.calculatePathsToActivate(
      [...this.devices].reverse(),
      [...this.switchesUpOn].reverse()
    );

    this.upSlots = this.pathHelper.calculateEachSlotColor(
      [...this.upSlots],
      [...pathsToActivate]
    );

    this.upSlots.map((slot: ISlot) =>
      slot.paths.map((path: IPath) => {
        if (pathsToActivate.includes(path.id)) {
          path.active = true;
        } else {
          path.active = false;
        }
      })
    );
    this.render = true;
  }
  downSwitch(emitted?: { value: boolean; switchIndex: number }) {
    if (emitted) {
      this.switchesDownOn[emitted.switchIndex] = emitted.value;
    }

    const pathsToActivate = this.pathHelper.calculatePathsToActivate(
      [...this.devices],
      [...this.switchesDownOn]
    );
    this.downSlots = this.pathHelper
      .calculateEachSlotColor(
        [...this.downSlots].reverse(),
        [...pathsToActivate].reverse()
      )
      .reverse();
    this.downSlots.map((slot: ISlot) =>
      slot.paths.map((path: IPath) => {
        if (pathsToActivate.includes(path.id)) {
          path.active = true;
        } else {
          path.active = false;
        }
      })
    );
    this.render = true;
  }

  ngOnDestroy() {
    this.dataPasser.mainInitialized$.next(false);
    this.timeSub.unsubscribe();
  }
}
