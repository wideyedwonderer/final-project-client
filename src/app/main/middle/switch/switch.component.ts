import { MinMaxSetterComponent } from './../min-max-setter/min-max-setter.component';
import { IPath } from '../../../models/path.interface';
import { ISlot } from './../arrows-tile/slot.interface';
import { IPosition } from './../arrows-tile/position.interface';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements AfterViewInit {
  @Input()
  slotsChanged: EventEmitter<boolean>;

  @Input()
  index: number;

  @ViewChild('canvas')
  canvas: ElementRef;

  @Input()
  deviceName: string;

  @Input()
  downSlot: ISlot;

  @Input()
  upSlot: ISlot;

  @Input()
  downFillColor;
  @Input()
  upFillColor;

  @Output()
  settingChange: EventEmitter<boolean> = new EventEmitter();

  @Output()
  downSwitchEmitter: EventEmitter<{value: boolean, switchIndex: number}> = new EventEmitter();
  downSwitchOn = true;
  @Output()
  upSwitchEmitter: EventEmitter<{value: boolean, switchIndex: number}> = new EventEmitter();
  upSwitchOn = true;

  mouseover = false;

  leftLinePosition: IPosition =  {x1: 0, x2: 0, y1: 0, y2: 0};
  rightLinePosition: IPosition = {x1: 0, x2: 0, y1: 0, y2: 0};

  rectangleWidth;
  rectangleXPosition;
  constructor(public dialog: MatDialog) {}

  ngAfterViewInit() {
    this.slotsChanged.subscribe(value => {

    });
    setTimeout(() => {
      this.calculateLinePositions(this.canvas.nativeElement);
      this.calculateRectanglePosition();
    }, 300);
  }

  upSlotActive() {
    let isActive = false;
    this.upSlot.paths.forEach((path) => {
      if (path.active) {
        isActive = true;
      }
    });
    return isActive;
  }

  downSlotActive() {
    let isActive = false;
    this.downSlot.paths.forEach((path) => {
      if (path.active) {
        isActive = true;
      }
    });
    return isActive;
  }

  downSwitchClick() {
    if ( this.downSlot.paths.length > 0) {
      this.downSwitchOn = !this.downSwitchOn;
      this.downSwitchEmitter.emit({value: this.downSwitchOn, switchIndex: this.index});
    }
    this.calculateRectanglePosition();
  }
  upSwitchClick() {
    if ( this.upSlot.paths.length > 0) {
      this.upSwitchOn = !this.upSwitchOn;
      this.upSwitchEmitter.emit({value: this.upSwitchOn, switchIndex: this.index});
    }
    this.calculateRectanglePosition();
  }
  openDialog(path): void {
    const dialogRef = this.dialog.open(MinMaxSetterComponent, {
      width: '160px',
      data: path
    });

    dialogRef.afterClosed().subscribe(result => this.settingChange.emit(true));
  }

  hoverValue() {
    this.mouseover = true;
  }

  mouseLeaveValue() {
    this.mouseover = false;
  }

  calculateRectanglePosition() {
    if (this.downSwitchOn && this.upSwitchOn) {
      this.rectangleWidth = (this.rightLinePosition.x1 - this.leftLinePosition.x1) + 60;
      this.rectangleXPosition = this.leftLinePosition.x1 - 30;
    } else if (!this.downSwitchOn && !this.upSwitchOn) {
      this.rectangleXPosition = this.leftLinePosition.x1 + 20;
      this.rectangleWidth = this.rightLinePosition.x2 - this.leftLinePosition.x2 - 40;
    } else if (!this.downSwitchOn) {
      this.rectangleXPosition = this.leftLinePosition.x1 + 20;
      this.rectangleWidth =  this.rightLinePosition.x2 - this.leftLinePosition.x2 + 10;
    } else {
      this.rectangleXPosition = this.leftLinePosition.x1 - 30;
      this.rectangleWidth =  this.rightLinePosition.x2 - this.leftLinePosition.x2 + 10;
    }
  }
  calculateLinePositions(canvas: HTMLElement) {

      const height = canvas.clientHeight;
      const width = canvas.clientWidth;
      this.leftLinePosition = {
      x1: width / 100 * 27,
      x2:  width /  100 * 27,
      y1: 0,
      y2: height
    };
    this.rightLinePosition = {
      x1: width /  100 * 73,
      x2:  width /  100 * 73,
      y1: 0,
      y2: height
    };
    this.calculateRectanglePosition();
  }


}
