import { ISlot } from './slot.interface';
import { IPosition } from './position.interface';
import { Component, Input, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MinMaxSetterComponent } from '../min-max-setter/min-max-setter.component';

@Component({
  selector: 'app-arrows-tile',
  templateUrl: './arrows-tile.component.html',
  styleUrls: ['./arrows-tile.component.scss']
})
export class ArrowsTileComponent implements AfterViewInit {

  fillColor = '#d3d3d3';
  @Input()
  index: number;

  @Input()
  downSlot: ISlot;

  @Input()
  upSlot: ISlot;

  @ViewChild('canvas')
  canvas: ElementRef;

  @Output()
  settingChange: EventEmitter<boolean> = new EventEmitter();

  mouseover = false;

  currentUpColor = 'black';
  currentDownColor = 'black';

  leftLinePosition: IPosition =  {x1: 0, x2: 0, y1: 0, y2: 0};
  rightLinePosition: IPosition = {x1: 0, x2: 0, y1: 0, y2: 0};

  constructor(public dialog: MatDialog) {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.calculateLinePositions(this.canvas.nativeElement);console.log(this.downSlot)
    }, 300);

  }

  openDialog(path): void {
    const dialogRef = this.dialog.open(MinMaxSetterComponent, {
      width: '160px',
      data: path
    });

    dialogRef.afterClosed().subscribe(result => {
      this.settingChange.emit(result);
    });
  }

  hoverValue() {
    this.mouseover = true;
  }

  upSlotActive() {
    let isActive = false;
    this.upSlot.paths.forEach((path) => {
      if (path.active) {
        isActive = true;
        this.currentUpColor = path.color;
      }
    });
    return isActive;
  }

  downSlotActive() {
    let isActive = false;
    this.downSlot.paths.forEach((path) => {
      if (path.active) {
        isActive = true;
        this.currentDownColor = path.color;
      }
    });
    return isActive;
  }

  mouseLeaveValue() {
    this.mouseover = false;
  }

  calculateMidArrowPosition(canvas) {
    return canvas.clientHeight / 2 - 20;
  }
  calculateLinePositions(canvas: HTMLElement) {

      const height = canvas.clientHeight;
      const width = canvas.clientWidth;

      this.leftLinePosition = {
      x1: width / 100 * 27,
      x2:  width /  100 * 27,
      y1: 0,
      y2: height
    };
    this.rightLinePosition = {
      x1: width /  100 * 73,
      x2:  width /  100 * 73,
      y1: 0,
      y2: height
    };
  }

}
