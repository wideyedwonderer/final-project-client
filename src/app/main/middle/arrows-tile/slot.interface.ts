import { IPath } from '../../../models/path.interface';

export interface ISlot {
  color: string;
  name: string;
  paths: Array<IPath>;
}
