import { IPath } from '../../../models/path.interface';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DbService } from '../../../../app/core/db.service';
import { PathSettings } from '../../../../app/core/path-settings-enum';

@Component({
  selector: 'app-min-max-setter',
  templateUrl: './min-max-setter.component.html',
  styleUrls: ['./min-max-setter.component.scss'],
})
export class MinMaxSetterComponent implements OnInit {
  min;
  max;
  constructor(
    public dialogRef: MatDialogRef<MinMaxSetterComponent>,
    @Inject(MAT_DIALOG_DATA) public path: IPath,
    private readonly dbService: DbService
  ) {}

  ngOnInit() {}
  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit() {
    this.dbService
      .setPathSetting(this.path.id, PathSettings.min, this.min)
      .subscribe();
    this.path.min = this.min;
    this.dbService
      .setPathSetting(this.path.id, PathSettings.max, this.max)
      .subscribe();
    this.path.max = this.max;
    this.dialogRef.close(this.settingChanged());
  }

  settingChanged() {
    if (this.min || this.max) {
      return true;
    } else {
      return false;
    }
  }
}
