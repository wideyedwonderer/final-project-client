import { DataPasserService } from './../../core/data-passer';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { mapStyles } from './map-styles';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  devices: Array<any> = [];
  styles: any;
  routes = [];
  lat = 42.664879;
  lng = 23.317603;
  render = false;
  constructor(private readonly dataPasser: DataPasserService, private readonly route: ActivatedRoute) {
    this.devices = this.route.snapshot.data['devices'];
    this.styles = mapStyles;
  }

  ngOnInit() {
    this.devices.map((device, index, array) => {
      if (index < this.devices.length - 1) {
        this.routes.push(
          {
            name: device.name + array[index + 1].name,
            origin: { lat: device.lat, lng: device.lng },
            destination: { lat: array[index + 1].lat, lng: array[index + 1].lng},
            renderOptions: { polylineOptions: { strokeColor: '#f00', strokeWeight: 9, clickable: true }, suppressMarkers: true }
          });
      }
    });
    this.render = true;
  }

}
