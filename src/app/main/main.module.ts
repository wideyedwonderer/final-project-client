import { MaterialRootModule } from './../common/material-root.module';
import { AddDeviceComponent } from './../admin/add-device/add-device.component';
import { AgmDirectionModule } from 'agm-direction';
import { AgmCoreModule } from '@agm/core';
import { NgIfOnceDirective } from './ngIf-once';
import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main-routing.module';
import { MiddleComponent } from './middle/middle.component';
import {
  MatGridListModule,
  MatFormFieldModule,
  MatDialogModule,
  MatInputModule,
  MatButtonModule,
} from '@angular/material';
import { ArrowsTileComponent } from './middle/arrows-tile/arrows-tile.component';
import { MatIconModule } from '@angular/material/icon';
import { SwitchComponent } from './middle/switch/switch.component';
import { CommonModule } from '@angular/common';
import { MinMaxSetterComponent } from './middle/min-max-setter/min-max-setter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MapComponent } from './map/map.component';
import { ChartsModule } from '../charts/charts.module';
import { ChartsContainerComponent } from './../charts/charts-container/charts-container.component';

@NgModule({
  declarations: [
    MainComponent,
    MiddleComponent,
    ArrowsTileComponent,
    SwitchComponent,
    NgIfOnceDirective,
    MinMaxSetterComponent,
    MapComponent,
    // ChartsContainerComponent,
  ],
  entryComponents: [MinMaxSetterComponent],
  imports: [
    MaterialRootModule,
    MainRoutingModule,
    MatGridListModule,
    MatIconModule,
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    AgmCoreModule,
    AgmDirectionModule,
    ChartsModule,
  ],
})
export class MainModule {}
