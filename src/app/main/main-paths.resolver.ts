import { IPath } from '../models/path.interface';
import { ApiService } from '../core/api.service';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DataPasserService } from '../core/data-passer';
import { DbService } from '../core/db.service';
import { Subscription, of, BehaviorSubject, Observable } from 'rxjs';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class TravelTimeDataResolver  implements Resolve<any> {

deviceNames: string[];
constructor(private readonly dataPasser: DataPasserService, private readonly db: DbService, private readonly api: ApiService) {
  this.dataPasser.currentRoute$.subscribe((route: any) => {
    const deviceNames = JSON.parse(route.devices).map((device) => device.name);
    this.deviceNames = deviceNames;
  });
}
  resolve(): Observable<any> {
      return this.api.getTravelTimeData(this.deviceNames, moment().subtract(2, 'hour'));
  }
}
