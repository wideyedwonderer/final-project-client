import { IPath } from '../models/path.interface';
import { ApiService } from '../core/api.service';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DataPasserService } from '../core/data-passer';
import { DbService } from '../core/db.service';
import { Subscription, of, BehaviorSubject, Observable } from 'rxjs';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class MainDevicesResolver  implements Resolve<any> {

devices: string[];
constructor(private readonly dataPasser: DataPasserService, private readonly db: DbService, private readonly api: ApiService) {
  this.dataPasser.currentRoute$.subscribe((route: any) => {
    const devices = JSON.parse(route.devices);
    this.devices = devices;
  });
}
  resolve(): Observable<string[]> {
      return of(this.devices);
  }
}
