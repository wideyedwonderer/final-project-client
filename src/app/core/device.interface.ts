export interface IDevice {
  id: string;
  name: string;
  lat: number;
  lng: number;
}
