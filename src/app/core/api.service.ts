import { DbService } from './db.service';
import { IPath } from '../models/path.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Moment } from 'moment';
import * as moment from 'moment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private readonly apiUrl: string;
  constructor(
    private readonly http: HttpClient,
    private readonly db: DbService
  ) {
    this.apiUrl = environment.apiUrl;
  }

  getTravelTimeData(deviceIds: string[], startDate: Moment) {
    const finalEndDate =  moment().subtract(5, 'minutes');
    const dateObject = `{"from":${startDate.valueOf()},"to":${finalEndDate.valueOf()}}`;
    return this.http
      .get(
        `${
          this.apiUrl
        }travelTimeTableData?devices=${deviceIds}&date=${dateObject}`
      )
      .pipe(
        map((data: any) => {
          const result: { down: IPath[]; up: IPath[] } = { down: [], up: [] };
          let toStartFrom = 0;

          Object.keys(data).forEach((key) => {
            Object.keys(data[key]).map((insideKey, i) => {
              if (key !== insideKey && i > toStartFrom) {
                const id: string = key.concat(`-${insideKey}`);
                const toPush: IPath = {
                  id,
                  min: null,
                  max: null,
                  current: data[key][insideKey],
                  active: false,
                };
                result.up.push(toPush);
              }
            });
            toStartFrom += 1;
          });
          toStartFrom = deviceIds.length;
          Object.keys(data)
            .reverse()
            .forEach((key) => {
              Object.keys(data[key]).map((insideKey, i) => {
                if (key !== insideKey && i < toStartFrom) {
                  const id: string = key.concat(`-${insideKey}`);
                  const toPush: IPath = {
                    id,
                    min: null,
                    max: null,
                    current: data[key][insideKey],
                    active: false,
                  };
                  result.down.push(toPush);
                }
              });
              toStartFrom -= 1;
            });
          return result;
        })
      );
  }
}
