import { Observable, Subject, of, BehaviorSubject } from 'rxjs';
import { User } from '../models/user.model';
import * as jwt_decode from 'jwt-decode';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { NotificatorService } from './notificator.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  currentUser$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  serverUrl: string;
  constructor(
    private readonly http: HttpClient,
    private readonly router: Router,
    private readonly notificator: NotificatorService
  ) {
    this.serverUrl = environment.serverUrl;
  }

  login(user: { email: string; password: string }): void {
    this.http.post(`${this.serverUrl}/auth/login`, user).subscribe({
      next: (token: string) => {
        localStorage.setItem('token', token);
        this.getUserByEmail(user.email).subscribe({
          next: (userData) => {
            const decodedToken = this.getDecodedAccessToken(token);
            userData = { ...userData, role: decodedToken.role };
            this.currentUser$.next(userData);
            localStorage.setItem('user', JSON.stringify(userData));
            this.router.navigate(['routes']);
          },
          error: () => {
            this.currentUser$.next(null);
            localStorage.removeItem('user');
          },
        });
        this.notificator.success('Successfully logged in');
        // this.router.navigate(['home']);
      },
      error: (error) => {
        this.currentUser$.next(null);
        this.notificator.error(error.error.message);
      },
    });
  }

  logout(): void {
    this.currentUser$.next(null);
    localStorage.clear();
    this.router.navigate(['home']);
  }

  register(user): Observable<any> {
    return this.http.post(`${this.serverUrl}/users`, user).pipe(
      catchError(() => {
        return of([]);
      })
    );
  }

  checkAuth(): void {
    const token = this.getDecodedAccessToken(localStorage.getItem('token'));
    if (token) {
      const tokendate = new Date(0);
      tokendate.setUTCSeconds(token.exp);
    }
    if (token && new Date(token.exp) <= new Date()) {
      this.getUserByEmail(token.email).subscribe((data) => {
        data = { ...data, role: token.role };
        console.log(data);
        this.currentUser$.next(data);
        localStorage.setItem('user', JSON.stringify(data));
        this.router.navigate(['routes']);
      });
    } else {
      this.currentUser$.next(null);
      localStorage.removeItem('token');
      localStorage.removeItem('user');
    }
  }

  getUserByEmail(email): Observable<User> {
    return this.http.get<User>(`${this.serverUrl}/users/${email}`);
  }

  private getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }
}
