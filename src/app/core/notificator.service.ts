import { Injectable } from '@angular/core';
import * as toastr from 'toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificatorService {

  success(msg: string, title?: string): void {
    toastr.success(msg, title);
  }

  error(err: string, title?: string): void {
    toastr.error(err, title);
  }
}
