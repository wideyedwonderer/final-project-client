import { ISlot } from './../main/middle/arrows-tile/slot.interface';
import { IPath } from '../models/path.interface';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PathHelperService {
  calculateWhereToPutNumbers(devices: string[], values: IPath[]): ISlot[] {
    let slots = Array.from({ length: devices.length - 1 + devices.length - 2 }, (x, i) => {
      const obj: ISlot = { name: '', color: '#3a3a2b', paths: [] };
      return obj;
    });

    let startPoint = 0;
    for (let i = devices.length - 1; i > 0; i--) {
      for (let j = 0; j < i; j++) {
        slots[startPoint + j].paths.push(values.shift());
      }
      startPoint += 2;
    }
    slots.push({ name: devices[devices.length - 1], color: '#3a3a2b', paths: [] });
    slots.unshift({ name: devices[0], color: '#3a3a2b', paths: [] });
    slots = slots.map((slot: ISlot, i) => {
      slot.name = i % 2 === 0 ? devices[i / 2] : devices[(i - 1) / 2].concat(`-${devices[(i + 1) / 2]}`);
      return slot;
    });
    return slots;
  }

  calculatePathsToActivate(devices: string[], switches: boolean[]) {
    const result = [];

    const setSwitches = (levelDeep, falseCount) => {
      if (levelDeep === switches.length - 1) {
        return;
      }
      if (switches[levelDeep + 1]) {
        result.push(devices[levelDeep - falseCount] + '-' + devices[levelDeep + 1]);
        setSwitches((levelDeep += 1), 0);
      } else {
        setSwitches((levelDeep += 1), (falseCount += 1));
      }
    };

    setSwitches(0, 0);
    return result;
  }

  calculatePathsColor(paths: IPath[]): IPath[] {
    const nPaths = paths.map((path: IPath) => {
      if (path.min && path.max) {
        if (path.current < path.min) {
          path.color = 'green';
        }
        if (path.current > path.min && path.current < path.max) {
          path.color = '#e2e20b';
        }
        if (path.current > path.max) {
          path.color = 'red';
        }
      }
      return path;
    });
    return nPaths;
  }

  calculateEachSlotColor(slots: ISlot[], activatedPaths: string[]): ISlot[] {
    const colors = Array.from({length: activatedPaths.length}, x => '');
    slots.map((slot: ISlot) => {
     activatedPaths.forEach((path: string, i) => {
           const pathToTakeColorFrom = slot.paths.filter((x) => x.id === path)[0];

           if (pathToTakeColorFrom) {

             colors[i] = pathToTakeColorFrom.color;
           }
     });
   });
     activatedPaths.map((path, i) => {
       const [fin, start] = path.split('-');
       let active = 0;
       slots.map((slot) => {

         if (slot.name === start) {
           active = 1;
         }
         if (slot.name === fin) {
           active = 0;
         }
         if (active) {
           slot.color = colors[i];
         }
       });
     });
     return slots;
   }
}
