import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DataPasserService {

currentRoute$: BehaviorSubject<any> = new BehaviorSubject(null);
currentDevices$: BehaviorSubject<any>;
timePicked$: BehaviorSubject<any> = new BehaviorSubject(null);
mainInitialized$: BehaviorSubject<any>;
constructor() {
  this.mainInitialized$  = new BehaviorSubject(false);
  this.currentRoute$.pipe(
    tap(value => console.log(value))
  );
}

}
