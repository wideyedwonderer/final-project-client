import { LogService } from './log.service';
import { UpdateDevice } from '../models/update-device.model';
import { AddDevice } from './../models/add-device.model';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AddReport } from '../models/add-report.model';
import { UpdateReport } from '../models/update-report.model';
import { PathSettings } from './path-settings-enum';
import { tap } from 'rxjs/operators';
import { NotificatorService } from './notificator.service';
import { Device } from '../admin/device.interface';

@Injectable({
  providedIn: 'root',
})
export class DbService {
  private readonly serverUrl: string;

  constructor(
    private readonly http: HttpClient,
    private readonly logger: LogService,
    private readonly notificator: NotificatorService
  ) {
    this.serverUrl = environment.serverUrl;
  }

  getAllDevices(userId: string) {
    return this.http.get(`${this.serverUrl}/devices?userId=${userId}`);
  }

  getAllReports(userId: string) {
    return this.http.get(`${this.serverUrl}/reports?userId=${userId}`);
  }

  addNewDevice(device: AddDevice) {
    this.logger
      .addNewLog('New Device', `Device name: ${device.name}`)
      .subscribe();
    return this.http.post(`${this.serverUrl}/devices`, device);
  }

  addNewReport(report: AddReport) {
    return this.http.post(`${this.serverUrl}/reports`, report);
  }

  updateDevice(deviceId: any, updateOptions: Device) {
    if (Object.keys(updateOptions).length === 0) {
      const logType = 'Remove Device';
      const logDetails = `Device name: ${updateOptions.name}`;
      this.logger.addNewLog(logType, logDetails).subscribe();
    }
    debugger;
    return this.http.post(
      `${this.serverUrl}/devices/${deviceId}`,
      updateOptions
    );
  }

  updateReport(reportId: string, updateOptions: UpdateReport) {
    return this.http.post(
      `${this.serverUrl}/reports/${reportId}`,
      updateOptions
    );
  }

  setPathSetting(pathId: string, pathSetting: PathSettings, value: number) {
    return this.http.post(`${this.serverUrl}/settings/${pathId}`, {
      setting: pathSetting,
      value,
    });
  }

  getPathsSettings(pathsIds: string[]) {
    return this.http.post(`${this.serverUrl}/settings`, pathsIds);
  }

  switchUserBan(userEmail, currentBan) {
    const logType = currentBan ? 'Banned user' : 'Remove user ban';
    const logDetails = `User Email: ${userEmail}`;
    this.logger.addNewLog(logType, logDetails).subscribe();
    return this.http.post(`${this.serverUrl}/users/${userEmail}`, {
      banned: !currentBan,
    });
  }

  getAllUsers() {
    return this.http.get(`${this.serverUrl}/users`);
  }

  addNewRoute(devices, name) {
    return this.http
      .post(`${this.serverUrl}/routes`, { devices, name })
      .pipe(tap((value) => this.notificator.success('Route added!')));
  }

  getAllRoutes() {
    return this.http.get(`${this.serverUrl}/routes`);
  }

  removeRoute(id) {
    return this.http.post(`${this.serverUrl}/routes/${id}`, {});
  }
}
