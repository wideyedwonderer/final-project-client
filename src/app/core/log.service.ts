import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';




@Injectable({
  providedIn: 'root',
})
export class LogService {
  serverUrl: string;
  constructor(
    private readonly http: HttpClient,
  ) {
    this.serverUrl = environment.serverUrl;
  }
addNewLog(type, description) {
  return this.http.post(`${this.serverUrl}/logs`, {type, description});
}

getAllLogs() {
  return this.http.get(`${this.serverUrl}/logs`);
}


}
