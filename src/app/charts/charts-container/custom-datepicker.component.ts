import { Component, Inject, ViewChild, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-custom-datepicker',
  templateUrl: 'custom-datepicker.component.html',
  styleUrls: ['custom-datepicker.component.scss'],
})
export class CustomDatePickerComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<CustomDatePickerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  public selectedDate;
  public today = new Date(new Date().setHours(0, 0, 0, 0));
  @ViewChild('selectedHours') selectedHours;
  @ViewChild('selectedMinutes') selectedMinutes;

  public hours = [
    { value: '00', viewValue: '00' },
    { value: '02', viewValue: '02' },
    { value: '04', viewValue: '04' },
    { value: '06', viewValue: '06' },
    { value: '08', viewValue: '08' },
    { value: '10', viewValue: '10' },
    { value: '12', viewValue: '12' },
    { value: '14', viewValue: '14' },
    { value: '16', viewValue: '16' },
    { value: '18', viewValue: '18' },
    { value: '20', viewValue: '20' },
    { value: '22', viewValue: '22' },
  ];

  public minutes = [
    { value: '00', viewValue: '00' },
    { value: '10', viewValue: '10' },
    { value: '20', viewValue: '20' },
    { value: '30', viewValue: '30' },
    { value: '40', viewValue: '40' },
    { value: '50', viewValue: '50' },
  ];
  public onSelect(data) {
    this.selectedDate = data;
  }

  public onCancelClick(): void {
    this.dialogRef.close();
  }
  public onOkClick(): void {
    this.dialogRef.close(
      Date.parse(this.selectedDate) +
        +this.selectedHours.value * 60 * 60 * 1000 +
        +this.selectedMinutes.value * 60 * 1000
    );
  }
  ngOnInit() {
    this.selectedHours.value = 0;
    this.selectedMinutes.value = 0;
  }
}
