import { Component, Inject, ViewChild, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { CustomDatePickerComponent } from './custom-datepicker.component';
import { DataPasserService } from '../../core/data-passer';

@Component({
  selector: 'app-new-chart-dialog',
  templateUrl: 'new-chart-dialog.component.html',
  styleUrls: ['new-chart-dialog.component.scss'],
})
export class NewChartDialogComponent implements OnInit {
  constructor(
    private readonly dataPasser: DataPasserService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<NewChartDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog
  ) {
    this.dataPasser.currentRoute$.subscribe((result) => {
      this.routeName = result.name;
      this.devices = JSON.parse(result.devices);
    });
  }
  public routeName;
  public devices;
  public period = 2;
  private origin: string;
  public destinationFilter = { value: 'string', viewValue: 'string' };
  public newChartForm = this.fb.group({
    origin: [''],
    destination: [''],
    period: [''],
    matButtonToggleGroup: [''],
    startDates: [''],
  });
  private datePattern = /^([0]\d|[1][0-2])\/([0-2]\d|[3][0-1])\/([2][01]|[1][6-9])\d{2}(\s([0-1]\d|[2][0-3])(\:[0-5]\d){1,2})?$/;
  public startDates: number[] = [];
  public fullDayStartDates = [];

  private routeDevices = [];
  @ViewChild('datePicker') datePicker;
  private today = new Date().setHours(0, 0, 0, 0);
  private yesterday = new Date(
    new Date().setDate(new Date().getDate() - 1)
  ).setHours(0, 0, 0, 0);
  public addFullDay(day) {
    if (day === 'today') {
      const todayIndex = this.fullDayStartDates.indexOf(this.today);
      if (todayIndex < 0) {
        this.fullDayStartDates.push(this.today);
      } else {
        this.fullDayStartDates.splice(todayIndex, 1);
      }
    } else {
      const yesterdayIndex = this.fullDayStartDates.indexOf(this.yesterday);
      if (yesterdayIndex < 0) {
        this.fullDayStartDates.push(this.yesterday);
      } else {
        this.fullDayStartDates.splice(yesterdayIndex, 1);
      }
    }
  }
  public showDataPicker() {
    this.datePicker.checked = false;
    const dialogRef = this.dialog.open(CustomDatePickerComponent, {
      width: '350px',
      height: '550px',
      data: {},
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && this.startDates.indexOf(result) < 0) {
        this.startDates.push(result);
      }
    });
  }
  public sendData() {
    this.dialogRef.close({
      origin: this.origin,
      destination: this.newChartForm.controls['destination'].value,
      startDates: this.startDates,
      period: this.period * 60 * 60 * 1000,
      fullDayStartDates: this.fullDayStartDates,
    });
  }
  public onNoClick() {
    this.dialogRef.close({});
  }
  private removeDate(date) {
    this.startDates.splice(this.startDates.indexOf(date), 1);
  }
  ngOnInit() {
    this.newChartForm
      .get('period')
      .valueChanges.subscribe((result) => (this.period = result));
    this.newChartForm.get('origin').valueChanges.subscribe((result) => {
      this.origin = result;
      this.destinationFilter.value = result;
      this.destinationFilter.viewValue = result;
    });
  }
}
