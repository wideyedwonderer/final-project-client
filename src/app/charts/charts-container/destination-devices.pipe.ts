import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'deviceFilter',
  pure: false,
})
export class DestinationDevicesPipe implements PipeTransform {
  transform(items: any[], filter: { value: string; viewValue: string }): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter((item) => item.name.indexOf(filter.value) < 0);
  }
}
