import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NewChartDialogComponent } from './new-chart-dialog.component';
import { PeriodsRequest } from './../periods-request.contract';
import { DataPasserService } from 'src/app/core/data-passer';
import { Device } from './../../admin/device.interface';

@Component({
  selector: 'app-charts-container',
  templateUrl: './charts-container.component.html',
  styleUrls: ['./charts-container.component.scss'],
})
export class ChartsContainerComponent implements OnInit {
  public charts: PeriodsRequest[] = [];
  constructor(
    public dialog: MatDialog,
    private readonly dataPasser: DataPasserService
  ) {
    this.dataPasser.currentRoute$.subscribe((result) => {
      this.devices = JSON.parse(result.devices);
    });
  }
  private devices;

  private id: number;
  private origin: string;
  private destination: string;
  private startDates: number[];
  private period: number;
  private fullDayStartDates: number[];
  private removeSingleChart(id) {
    this.charts.splice(this.charts.indexOf(this.charts[id - 1]), 1);
  }
  public addNewChart() {
    const dialogRef = this.dialog.open(NewChartDialogComponent, {
      data: {
        id: this.id,
        origin: this.origin,
        destination: this.destination,
        startDates: this.startDates,
        period: this.period,
        fullDayStartDates: this.fullDayStartDates,
      },
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result === undefined) {
        return;
      }
      result.id = this.charts.length + 1;
      this.charts.push(result);
    });
  }
  private addDefaultChart() {
    const period = Date.now() - new Date(Date.now()).setHours(0, 0, 0, 0);
    const data = {
      id: 1,
      origin: this.devices[0].name,
      destination: this.devices[this.devices.length - 1].name,
      startDates: [new Date().setHours(0, 0, 0, 0)],
      period: period,
      fullDayStartDates: [],
    };
    this.charts.push(data);
  }
  ngOnInit() {
    this.addDefaultChart();
  }
}
