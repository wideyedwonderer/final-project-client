export interface PeriodsRequest {
  id: number;
  origin: string;
  destination: string;
  startDates: number[];
  period: number;
}
