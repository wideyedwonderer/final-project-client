import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SingleChartComponent } from './single-chart/single-chart.component';
import { ChartsContainerComponent } from './charts-container/charts-container.component';
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatButtonToggleModule,
  MatSliderModule,
  MatDatepickerModule,
  MatCardModule,
  MatNativeDateModule,
  MatOptionModule,
  MatSelectModule,
  MatTooltipModule,
} from '@angular/material';
import { NewChartDialogComponent } from './charts-container/new-chart-dialog.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DestinationDevicesPipe } from './charts-container/destination-devices.pipe';
import { CustomDatePickerComponent } from './charts-container/custom-datepicker.component';

@NgModule({
  declarations: [
    SingleChartComponent,
    ChartsContainerComponent,
    NewChartDialogComponent,
    DestinationDevicesPipe,
    CustomDatePickerComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonToggleModule,
    MatIconModule,
    MatSliderModule,
    FormsModule,
    MatDatepickerModule,
    MatCardModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatOptionModule,
    MatTooltipModule,
  ],
  entryComponents: [NewChartDialogComponent, CustomDatePickerComponent],
  providers: [],
  exports: [ChartsContainerComponent, SingleChartComponent],
})
export class ChartsModule {}
