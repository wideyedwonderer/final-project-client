import { Component, NgZone, Input } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { ChartsService } from '../charts.service';

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-single-chart',
  templateUrl: './single-chart.component.html',
  styleUrls: ['./single-chart.component.scss'],
  providers: [ChartsService],
})
export class SingleChartComponent {
  constructor(private zone: NgZone, private chartsService: ChartsService) {}
  private chart: am4charts.XYChart;
  @Input() chartParams: any;

  private getChartBase() {
    if (this.chartParams.fullDayStartDates.length > 0) {
      this.chartsService
        .getDevicePeriods(
          this.chartParams.origin,
          this.chartParams.destination,
          this.chartParams.fullDayStartDates,
          86400000
        )
        .subscribe((res) => {
          this.getChartPeriods(res);
        });
    } else {
      this.getChartPeriods({});
    }
  }
  private async getChartPeriods(chartBase) {
    if (this.chartParams.startDates.length === 0) {
      this.chartsService.createChart(
        this.chartParams.id,
        chartBase,
        this.chartParams.origin,
        this.chartParams.destination
      );
    } else {
      this.chartsService
        .getDevicePeriods(
          this.chartParams.origin,
          this.chartParams.destination,
          this.chartParams.startDates,
          this.chartParams.period
        )
        .subscribe((res: any) => {
          this.chartsService.createChart(
            this.chartParams.id,
            Object.assign({}, chartBase, res),
            this.chartParams.origin,
            this.chartParams.destination
          );
        });
    }
  }
  private async createChartDataObject() {
    await this.getChartPeriods(await this.getChartBase());
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this.getChartBase();
    });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
