import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChartsContainerComponent } from './charts-container/charts-container.component';

const routes: Routes = [
  { path: 'rightside', component: ChartsContainerComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChartsRoutingModule {}
