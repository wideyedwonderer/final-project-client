import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import * as moment from 'moment';

@Injectable()
export class ChartsService {
  public constructor(private http: HttpClient) {}
  public getDevicePeriods(
    originDeviceId: string,
    destinationDeviceId: string,
    startDates: number[],
    period: number
  ) {
    const url = `http://ec2-35-158-53-19.eu-central-1.compute.amazonaws.com:8080`;

    return this.http.get(
      `${url}/api/comparePeriods` +
        `?originDeviceId=${originDeviceId}` +
        `&destinationDeviceId=${destinationDeviceId}` +
        `&startDates=${startDates}` +
        `&periodLength=${period}`
    );
  }
  public createChart(id, data, origin, destination) {
    const divElementName = 'chartdiv' + id;
    const chart = am4core.create(divElementName, am4charts.XYChart);
    const timeAxis = chart.xAxes.push(new am4charts.DateAxis());
    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    const title = chart.titles.create();
    title.text = `${origin} - ${destination}`;
    title.fontSize = 20;
    title.marginBottom = 10;
    const keys = Object.keys(data);
    const startHours: object = keys.reduce((acc, el): any => {
      acc[el] =
        (new Date(+el).getHours() * 60 + new Date(+el).getMinutes()) *
        60 *
        1000;
      return acc;
    }, {});
    const startHoursSorted = Object.entries(startHours).sort(
      (a: any, b: any): any => {
        return a[1] - b[1];
      }
    );
    const startPoint = startHoursSorted[0][0];
    chart.colors.step = 3;
    chart.legend = new am4charts.Legend();
    chart.cursor = new am4charts.XYCursor();
    // tslint:disable-next-line:forin
    for (const key in data) {
      const leveler: number = +key - +startPoint - +startHours[key];
      createAxisAndSeries(
        leveler,
        data[key],
        moment(+key).format('DD/MM/YYYY'),
        true,
        'Circle'
      );
    }
    function createAxisAndSeries(
      leveler,
      seriesData,
      name,
      opposite,
      bulletType
    ) {
      const series = new am4charts.LineSeries();
      timeAxis.dataFields.data = 'x';
      series.data = seriesData;
      series.data.map((el) => {
        el.x -= leveler;
      });
      series.dataFields.valueY = 'y';
      series.dataFields.dateX = 'x';
      series.strokeWidth = 2;
      series.yAxis = valueAxis;
      // series.fillOpacity = 0.6;
      series.name = name;
      series.tooltipText = '{name}: [bold]{valueY}[/]';
      series.tensionX = 0.8;
      const interfaceColors = new am4core.InterfaceColorSet();
      const point = series.bullets.push(new am4charts.Bullet());
      const bullet = point.createChild(am4core[bulletType]);
      bullet.width = 6;
      bullet.height = 6;
      bullet.stroke = interfaceColors.getFor('background');
      bullet.horizontalCenter = 'middle';
      bullet.verticalCenter = 'middle';
      valueAxis.renderer.line.strokeOpacity = 1;
      valueAxis.renderer.line.strokeWidth = 1;
      valueAxis.renderer.line.stroke = series.stroke;
      valueAxis.renderer.labels.template.fill = series.stroke;
      valueAxis.renderer.opposite = opposite;
      valueAxis.renderer.grid.template.disabled = true;
      chart.series.push(series);
    }
  }
}
