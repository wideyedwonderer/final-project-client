import { EventEmitter } from '@angular/core';
export interface IMouseOver {
  mouseOver: boolean;
  mouseOverEmitter: EventEmitter<any>;
  mouseLeaveEmitter: EventEmitter<any>;

}
