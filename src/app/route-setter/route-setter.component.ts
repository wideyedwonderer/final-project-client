import { AreYouSureComponent } from './../common/are-you-sure/are-you-sure.component';
import { MatTable, MatDialog } from '@angular/material';
import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DbService } from '../core/db.service';
import { throttleTime } from 'rxjs/operators';
import { IMouseOver } from './mouse-over.interface';
import { DataPasserService } from '../core/data-passer';
import { mapStyles } from '../main/map/map-styles';

@Component({
  selector: 'app-route-setter',
  templateUrl: './route-setter.component.html',
  styleUrls: ['./route-setter.component.scss'],
})
export class RouteSetterComponent implements OnInit {
  removeClicked = false;
  styles: any;
  devices = [];
  devicesToAdd = [];
  direction = {
    name: null,
    origin: { lat: 0, lng: 0 },
    destination: { lat: 0, lng: 0 },
    renderOptions: {
      polylineOptions: {
        strokeColor: 'purple',
        strokeWeight: 9,
        clickable: true,
      },
      suppressMarkers: true,
    },
    waypoints: [],
  };
  removable = true;
  routeName;
  routes;
  displayedColumnsRoutes = ['name', 'devicesCount', 'remove'];
  mouseOvers: IMouseOver[] = [];
  @ViewChild(MatTable) table: MatTable<any>;
  constructor(
    private readonly route: ActivatedRoute,
    private readonly db: DbService,
    public dialog: MatDialog,
    private readonly router: Router,
    private readonly dataPasser: DataPasserService
  ) {
    this.styles = mapStyles;
  }

  ngOnInit() {
    this.devices = this.route.snapshot.data['devices'];
    this.routes = this.route.snapshot.data['routes'];
    this.routes = this.routes.map((route, i) => {
      route = { ...route, devicesCount: JSON.parse(route.devices).length };

      return route;
    });
    this.setMouseOvers();
  }

  markerClick(deviceToAdd) {
    let deviceAlreadyAdded = false;
    this.devicesToAdd.map((device) => {
      if (device.deviceId === deviceToAdd.deviceId) {
        deviceAlreadyAdded = true;
      }
    });
    if (!deviceAlreadyAdded) {
      this.devicesToAdd.push(deviceToAdd);
      this.renderDirections();
    }
  }

  removeDevice(device) {
    const index = this.devicesToAdd.indexOf(device);
    if (index >= 0) {
      this.devicesToAdd.splice(index, 1);
      this.renderDirections();
    }
  }
  renderDirections() {
    const direction = {
      origin: { lat: null, lng: null },
      destination: { lat: null, lng: null },
      renderOptions: null,
      name: null,
      waypoints: [],
    };
    if (this.devicesToAdd.length > 1) {
      const firstDevice = this.devicesToAdd[0];
      const lastDevice = this.devicesToAdd[this.devicesToAdd.length - 1];
      if (this.devicesToAdd.length > 2) {
        this.devicesToAdd.map((device, index) => {
          if (index !== 0 && index !== this.devicesToAdd.length - 1) {
            direction.waypoints.push({
              location: { lat: device.lat, lng: device.lng },
            });
          }
        });
      }
      direction.origin.lat = firstDevice.lat;
      direction.origin.lng = firstDevice.lng;
      direction.destination.lat = lastDevice.lat;
      direction.destination.lng = lastDevice.lng;
      direction.renderOptions = {
        polylineOptions: {
          strokeColor: 'purple',
          strokeWeight: 9,
          clickable: true,
        },
        suppressMarkers: true,
      };
    }
    this.direction = direction;
  }

  addRoute() {
    this.db
      .addNewRoute(this.devicesToAdd, this.routeName)
      .subscribe((result: any) => {
        this.routes.push({
          ...result,
          devicesCount: JSON.parse(result.devices).length,
        });
        this.table.renderRows();
        this.devicesToAdd.splice(0, this.devicesToAdd.length);
        this.routeName = '';
        this.setMouseOvers();
      });
  }

  submitDisabled() {
    return !(!!this.routeName && this.devicesToAdd.length > 1);
  }

  removeRoute(id) {
    this.removeClicked = true;
    const dialogRef = this.dialog.open(AreYouSureComponent, {
      width: '250px',
    });
    dialogRef.afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        this.db.removeRoute(id).subscribe((result: any) => {
          if (result.raw.affectedRows) {
            this.routes = this.routes.filter((route) => route.routeId !== id);
            this.table.renderRows();
          }
        });
      }
    });
    setTimeout(() => {
      this.removeClicked = false;
    }, 100);
  }

  setMouseOvers() {
    this.mouseOvers = Array.from({ length: this.routes.length }, undefined);

    this.routes.forEach((route, i) => {
      this.mouseOvers[i] = {
        mouseOver: false,
        mouseOverEmitter: new EventEmitter(),
        mouseLeaveEmitter: new EventEmitter(),
      };
      this.mouseOvers[i].mouseOverEmitter
        .pipe(throttleTime(500))
        .subscribe((mouseOveredRoute) => {
          const devices = JSON.parse(mouseOveredRoute.devices);
          this.devicesToAdd = [...devices];
          this.renderDirections();
          this.mouseOvers[i].mouseOver = true;
        });
      this.mouseOvers[i].mouseLeaveEmitter.subscribe((mouseOveredRoute) => {
        this.devicesToAdd = [];
        this.renderDirections();
        this.mouseOvers[i].mouseOver = false;
      });
    });
  }
  onMouseOver(event, i) {
    this.mouseOvers[i].mouseOverEmitter.emit(event);
  }
  onMouseLeave(event, i) {
    this.mouseOvers[i].mouseLeaveEmitter.emit(event);
  }

  activateRoute(row) {
    setTimeout(() => {
      if (!this.removeClicked) {
        this.dataPasser.currentRoute$.next(row);
        this.router.navigate(['main']);
      }
    }, 20);
  }
}
