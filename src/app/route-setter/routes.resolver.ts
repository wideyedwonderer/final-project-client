import { DbService } from './../core/db.service';

import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class RoutesResolver  implements Resolve<any> {


constructor(private readonly db: DbService) {}
  resolve(): Promise<any> {
        return  this.db.getAllRoutes().toPromise().then((data: any) => data);
  }
}
