import { UserRegisterComponent } from './auth/register/user-register.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserLoginComponent } from './auth/login/user-login.component';
import { RouteSetterComponent } from './route-setter/route-setter.component';
import { DevicesResolver } from './admin/resolvers/devices.resolver';
import { RoutesResolver } from './route-setter/routes.resolver';
import { TravelTimeDataResolver } from './main/main-paths.resolver';
import { MainDevicesResolver } from './main/main-devices.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: UserLoginComponent },
  { path: 'register', component: UserRegisterComponent },
  { path: 'main', loadChildren: './main/main.module#MainModule', resolve: { paths: TravelTimeDataResolver, devices: MainDevicesResolver } },
  { path: 'routes', component: RouteSetterComponent, resolve: { devices: DevicesResolver, routes: RoutesResolver }},
  // { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
