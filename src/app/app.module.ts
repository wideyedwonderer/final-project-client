import { environment } from 'src/environments/environment';
import { UserRegisterComponent } from './auth/register/user-register.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserLoginComponent } from './auth/login/user-login.component';
import { MaterialRootModule } from './common/material-root.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpAuthInterceptor } from './common/http.interceptor';
import { ChartsModule } from './charts/charts.module';
import { ChartsRoutingModule } from './charts/charts-routing.module';
import { AgmDirectionModule } from 'agm-direction';
import { AgmCoreModule } from '@agm/core';
import { NavComponent } from './nav/nav.component';
import { SharedModule } from './common/shared.module';
import { RouteSetterComponent } from './route-setter/route-setter.component';
import { AreYouSureComponent } from './common/are-you-sure/are-you-sure.component';


@NgModule({
  declarations: [AppComponent, UserLoginComponent, NavComponent, RouteSetterComponent],
  imports: [
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialRootModule,
    ChartsModule,
    ChartsRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMaps,
    }),
    AgmDirectionModule
  ],
  exports: [UserRegisterComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpAuthInterceptor, multi: true },
  ],
  entryComponents: [
    AreYouSureComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
