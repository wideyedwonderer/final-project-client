import { Subscription } from 'rxjs';
import { DataPasserService } from './../core/data-passer';
import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../core/user.service';
import { MatButtonToggleGroup } from '@angular/material';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  isAdmin;
  mainInitialized = false;
  constructor(
    private readonly userService: UserService,
    private readonly dataPasser: DataPasserService
  ) {}
  @ViewChild('group') group: MatButtonToggleGroup;

  ngOnInit() {
    this.userService.currentUser$.subscribe((user) => {
      if (user) {
        this.isAdmin = user.role === 'admin';
      }
    });
    this.group.valueChange.subscribe((data) =>
      this.dataPasser.timePicked$.next(data)
    );
    this.dataPasser.mainInitialized$.subscribe((value) => {
      if (value) {
        this.mainInitialized = true;
      } else {
        this.mainInitialized = false;
      }
    });
  }

  logout() {
    this.userService.logout();
  }
}
